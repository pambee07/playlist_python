class Artist:
    def __init__(self, first_name, last_name):
        # Initialise un nouvel artiste avec un prénom et un nom
        self.first_name = first_name
        self.last_name = last_name

    def get_full_name(self):
        # Retourne le nom complet de l'artiste
        return f"{self.first_name} {self.last_name}"

class Music:
    def __init__(self, title, duration, artist_set):
        # Initialise une nouvelle musique
        self.title = title
        self.duration = duration
        self.artist_set = artist_set

    def get_infos(self):
        # Retourne les informations sur la musique (titre, durée, artistes)
        artists = ", ".join([artist.get_full_name() for artist in self.artist_set])  # Concatène les noms des artistes
        minutes = self.duration // 60  # Calcule les minutes
        seconds = self.duration % 60  # Calcule les secondes restantes
        return f"Title: {self.title}, Duration: {minutes:02d}:{seconds:02d}, Artists: {artists}"

class Playlist:
    def __init__(self):
        # Initialise une nouvelle playlist avec une musique en cours
        self.current_music = None
        self.music_list = []

    def add(self, music):
        # Ajoute une musique à la liste de musiques de la playlist
        self.music_list.append(music)

    def remove(self, position):
        # Retire une musique de la liste de musiques
        if 0 <= position < len(self.music_list):
            del self.music_list[position]

    def get_total_duration(self):
        # Calcule la durée totale de la playlist (en secondes)
        total_duration = sum([music.duration for music in self.music_list])  # Somme des durées de toutes les musiques
        return total_duration

    def next(self):
        # Passe à la musique suivante dans la liste
        if self.current_music is None:  # Si aucune musique n'est en cours
            if self.music_list:  # S'il y a des musiques dans la liste
                self.current_music = self.music_list[0]  # Définit la première musique comme musique en cours
        else:  # Si une musique est en cours
            try:
                index = self.music_list.index(self.current_music)  # Trouve l'indice de la musique en cours dans la liste
                self.current_music = self.music_list[index + 1]  # Définit la musique suivante comme musique en cours
            except IndexError:  # Si la musique en cours est la dernière de la liste
                self.current_music = None  # Définit aucune musique comme musique en cours

class Player:
    def main(self):
        # Méthode principale pour tester les classes
        # Crée des instances des classes Artist, Music et Playlist
        artist1 = Artist("Armin", "Van Buuren")
        artist2 = Artist("David", "Guetta")
        artist3 = Artist("Yoel" , "Lewis")
        artist4 = Artist("Martin" , "Garrix")
        artist5 = Artist("Martin" , "Solveig")
        music1 = Music("I live for that Energy", 180, {artist1})
        music2 = Music("The World is Mine", 240, {artist2})
        music3 = Music("Caesarea", 150, {artist3})
        music4 = Music("Animals", 250, {artist4})
        music5 = Music("Jalousy", 270, {artist5})
        playlist = Playlist()

        # Ajoute les musiques à la playlist
        playlist.add(music1)
        playlist.add(music2)
        playlist.add(music3)
        playlist.add(music4)
        playlist.add(music5)

        playlist.current_music = music3
        
        # Teste les méthodes des différentes classes et affiche les résultats
        print("Infos de la première musique de la playlist :")
        print(music3.get_infos())

        print("\nDurée totale de la playlist (en secondes) :")
        print(playlist.get_total_duration())

        print("\nMusique suivante dans la playlist :")
        playlist.next()
        if playlist.current_music:  # Si une musique est en cours
            print(playlist.current_music.get_infos())  # Affiche les informations sur la musique en cours
        else:  # Si aucune musique n'est en cours
            print("Aucune musique en cours")

if __name__ == "__main__":
    player = Player()
    player.main()
